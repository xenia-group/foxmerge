#!/usr/bin/env python3

import os
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "foxmerge",
    version = "1.0.0",
    author = "Mia Neufeld",
    author_email = "mia@xenialinux.com",
    description= ("Portage wrapper for Xenia."),
    license = "GPL-3",
    keywords = "package manager",
    url = "https://gitlab.com/xenia-group/foxmerge",
    packages = ["foxmerge", "foxmerge.fuzzysearch"],
    requires=["foxcommon", "sys", "portage", "argparse"],
    entry_points={
        'console_scripts': [
            'foxmerge = foxmerge.foxmerge:main',
        ],
    },
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Package"
        "License :: OSI Approved :: GPL-3"
    ],
)
