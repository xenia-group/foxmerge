#!/usr/bin/env python3

import fuzzywuzzy
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import os


def fuzzy_package_search(query):
    portage_root = "/var/db/repos/"
    portage_root_dir = os.fsencode(portage_root)

    forbidden_files = [
        "README.md",
        "metadata",
        "profiles",
        "repositories.xml",
        "LICENSE",
        "licenses",
        "header.txt",
        "skel.ebuild",
        "skel.metadata.xml",
        "Manifest",
        "Manifest.files.gz",
        "scripts",
        "eclass",
        ".editorconfig",
        ".git",
        ".gitignore",
        "FAQ.md",
    ]

    possible_matches = []

    for repos in os.listdir(portage_root_dir):
        repo_name = os.fsdecode(repos)

        # Yes, I hate this
        # I need to figure out how to filter out READMEs, metadata, etc.
        # I don't want to.
        repodir_as_string = portage_root + repo_name
        for category in os.listdir(repodir_as_string):
            category_name = os.fsdecode(category)

            category_dir_as_string = repodir_as_string + "/" + category_name
            if category_name not in forbidden_files and os.path.isdir(
                category_dir_as_string
            ):
                for package in os.listdir(category_dir_as_string):
                    package_name = os.fsdecode(package)

                    if fuzz.ratio(package_name, query) >= 80:
                        possible_matches.append(
                            category_name + "/" + package_name + "::" + repo_name
                        )

    return possible_matches
