#!/usr/bin/env python3

import foxsnapshot as fs
import foxcommon as fc
import portage
import argparse
import foxmerge.fuzzysearch as fuzsearch
import os
import sys
import random
import subprocess

from datetime import datetime


def mount_usr():
    fc.execute("mount -o rw,remount /usr")
    

def umount_usr():
    fc.execute("mount -o ro,remount /usr")


def checks():
    if os.geteuid() != 0:
        fc.die("foxmerge requires root privileges to run.")

    if fs.btrfs():
        fs.snapshot_checks()
        return True


def update():
    fc.info("Running emerge --sync. May take a while.")
    fc.execute("emerge --sync")


def upgrade(is_btrfs):
    mount_usr()

    if is_btrfs:
        snapshot_name = f"{datetime.today().strftime('%Y-%m-%d_%H:%M:%S')}_fm_upg"
        fs.create_snapshot(snapshot_name)

    try:
        subprocess.run(
            "emerge -quUa --autounmask-write=y --backtrack=50 @xenia",
            shell=True,
            check=True,
        )
    except subprocess.CalledProcessError:
        fc.warn(
            "Upgrade failed. " + f"Reverting to previous snapshot {snapshot_name}.."
            if is_btrfs
            else ""
        )
        fs.revert_snapshot(snapshot_name)
        return

    if is_btrfs:
        fs.create_snapshot(snapshot_name)
        
    umount_usr()


def search(*packages):
    for package in packages:
        query_result = fuzsearch.fuzzy_package_search(package)
        fc.info(f"Possible matches for {package}: ")
        for package in query_result:
            print(package)


def check_for_valid_package_atom(*packages):
    portage_db = portage.db[portage.root]["porttree"].dbapi

    valid_packages = []
    for package_list in packages:
        for package in package_list[0]:
            query_result = portage_db.xmatch("bestmatch-visible", package)
            if query_result != "":
                fc.info(f"Found package {query_result}")
                split = portage.pkgsplit(query_result)
                valid_packages.append(split[0])
            else:
                fc.info(f"Unable to find visible (unmasked) {package}, checking for masked versions")
                query_result = portage_db.xmatch("match-all", package)
                result_length = len(query_result)
                if result_length != 0:
                    if '9999' not in query_result[result_length-1]:
                        package = query_result[result_length-1]
                    else:
                        if query_result[result_length-2] != "":
                            package = query_result[result_length-2]
                    fc.info(f"Found package {package}")
                    split = portage.pkgsplit(package)
                    valid_packages.append(split[0])
                else:
                    closest_match = fuzsearch.fuzzy_package_search(package)
                    fc.die(
                        f"Unable to find package {package}, did you mean {closest_match}?"
                    )

    fc.info(
        f"List of all valid package atoms found: {valid_packages}\n Is this correct? y/n"
    )

    unresolved = True
    while unresolved:
        answer = input()
        if answer in ["y", "Y", "yes", "Yes"]:
            return valid_packages
        elif answer in ["n", "N", "no", "No"]:
            exit()
        else:
            fc.warn("Answer, not understood, please repeat: ")


def add_package(*packages):
    if not os.path.exists("/etc/portage/sets/xenia"):
        fc.info("Xenia package set does not exist, creating set")
        fc.execute("mkdir -p /etc/portage/sets")
        fc.execute("touch /etc/portage/sets/xenia")
        
    sets_file = open("/etc/portage/sets/xenia", "a")
    xenia_file = open("/etc/portage/sets/xenia", "r")

    fc.info("Checking for valid package atoms")
    valid_packages = check_for_valid_package_atom(packages)

    xenia = [pkg.strip() for pkg in xenia_file.readlines()]

    for package in valid_packages:
        if package not in xenia:
            fc.info(f"Recording {package} to Xenia set...")
            sets_file.write(f"{package}\n")
        else:
            fc.info(f"Package {package} already in Xenia set, skipping..")

    sets_file.close()
    xenia_file.close()

    fc.info(
        "\nSuccessfully added all packages, please use foxmerge upgrade to install the new packages"
    )


def remove_packages(is_btrfs, *packages):
    mount_usr()

    if is_btrfs:
        snapshot_name = f"{datetime.today().strftime('%Y-%m-%d_%H:%M:%S')}_fm_rem"
        fs.create_snapshot(snapshot_name)

    valid_packages = check_for_valid_package_atom(packages)

    fc.info(f"Do you want to remove {valid_packages} from your system?")

    unresolved = True
    while unresolved:
        answer = input()
        if answer in ["n", "N", "no", "No"]:
            exit()
        if answer in ["y", "Y", "yes", "Yes"]:
            unresolved = False
        else:
            fc.info(f"Response {answer} not understood")

    new_packages = []

    with open("/etc/portage/sets/xenia", "r") as packages:
        new_packages = [
            f"{pkg}\n"
            for pkg in [pkg1.strip() for pkg1 in packages.readlines()]
            if pkg not in valid_packages
        ]

    with open("/etc/portage/sets/xenia", "w") as xenia_set:
        xenia_set.writelines(new_packages)

    fc.info("Removing packages from the system now")

    try:
        subprocess.run("emerge -a --depclean", shell=True, check=True)
    except subprocess.CalledProcessError:
        fc.warn(
            "Removal failed. " + f"Reverting to previous snapshot {snapshot_name}.."
            if is_btrfs
            else ""
        )
        fs.revert_snapshot(snapshot_name)
        return

    if is_btrfs:
        fs.create_snapshot(snapshot_name)
        
    umount_usr()


def list_packages():
    fc.info("List of packages currently in the Xenia Set")
    with open("/etc/portage/sets/xenia", "r") as packages:
        for package in packages.readlines():
            print(package.strip())


def handle_args():
    parser = argparse.ArgumentParser(
        prog="foxmerge", description="portage wrapper for Xenia Linux"
    )
    subparser = parser.add_subparsers(
        help="Action to take.", required=True, dest="action"
    )

    search_parser = subparser.add_parser(
        "search", help="Searches for the specified package"
    )
    add_parser = subparser.add_parser("add", help="Adds package to the @xenia set")
    remove_parser = subparser.add_parser(
        "remove", help="Removes package from the @xenia set and uninstalls them"
    )
    subparser.add_parser("list", help="Lists packages in the @xenia set")
    subparser.add_parser(
        "update",
        help="Syncs the local package repositories with the remote repositories",
    )
    subparser.add_parser(
        "upgrade", help="Upgrades the system and installs new packages"
    )

    search_parser.add_argument("packagename", help="Name of packages to search for")
    add_parser.add_argument(
        "packages", help="Name of packages to add to the @xenia set", nargs="+"
    )
    remove_parser.add_argument(
        "packages", help="Name of packages to be removed from the @xenia set", nargs="+"
    )

    args = parser.parse_args()
    return vars(args)


# This function just exists so someone could run 'foxmerge emacs' for example, and just oneshot that.
def oneshot_package(package):
    valid_package = check_for_valid_package_atom(package)
    fc.execute(f"emerge -1 {valid_package}")

def initial_setup():
    if not os.path.isdir("/etc/portage/sets"):
        fc.info("Creating portage sets directory")
        os.mkdir("/etc/portage/sets")

    if not os.path.isfile("/etc/portage/sets/xenia"):
        fc.info("Creating @xenia set")
        open("/etc/portage/sets/xenia", "a").close()

    if not os.path.isdir("/var/db/repos/xenia-overlay"):
        fc.info("Adding Xenia overlay")
        fc.execute("eselect repository add xenia-overlay git https://xenia-group/xenia-overlay.git")

    if not os.path.isdir("/var/db/repos/gentoo/profiles/"):
        fc.info("Running initial update")
        update()

def main():
    args = handle_args()
    initial_setup()

    # Search and List shouldn't require root privileges
    if args["action"] not in ["search", "list"]:
        # Attempting to create a btrfs snapshot on a non-btrfs system is kinda dummie
        is_btrfs = checks()

    match args["action"]:
        case "search":
            search(args["packagename"])
        case "add":
            add_package(args["packages"])
        case "remove":
            remove_packages(is_btrfs, args["packages"])
        case "list":
            list_packages()
        case "update":
            update()
        case "upgrade":
            upgrade(is_btrfs)
        case _:
            oneshot_package(args)


if __name__ == "__main__":
    main()
